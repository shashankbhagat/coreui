
<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="resolvebiz">
  <meta name="author" content="resolvebiz">
  <meta name="keyword" content="resolvebiz">
  <title>CoreUI Pro Bootstrap Admin Template</title>

  <link href="{{asset('coreui/vendors/coreui/icons/css/coreui-icons.min.css')}}" rel="stylesheet">
  <link href="{{asset('coreui/vendors/flag-icon-css/css/flag-icon.min.css')}}" rel="stylesheet">
  <link href="{{asset('coreui/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('coreui/vendors/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet">

  <link href="{{asset('coreui/css/style.css')}}" rel="stylesheet">
  <link href="{{asset('coreui/vendors/pace-progress/css/pace.min.css')}}" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
  <header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
      <img class="img-avatar navbar-brand-full" src="{{asset('img/resolve_logo.png')}}" width="89" height="25" alt="CoreUI Logo">
      <img class="navbar-brand-minimized" src="{{asset('img/resolve_logo.png')}}" width="30" height="30" alt="CoreUI Logo">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <!-- @include('sidebar.topBarMenus') -->
    <ul class="nav navbar-nav ml-auto">
      <!-- @include('sidebar.topBarNotifications') -->
      <li class="nav-item dropdown" style="margin-right: 35px;">
        <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <img class="img-avatar" src="{{asset('img/resolve_logo.png')}}" alt="admin@bootstrapmaster.com">
        </a>
        @include('sidebar.profileMenu')
      </li>
    </ul>
    <!-- below button is for right sidebar starts here -->
    <!-- <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
      <span class="navbar-toggler-icon"></span>
    </button> -->
    <!-- <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
      <span class="navbar-toggler-icon"></span>
    </button> -->
    <!-- below button is for right sidebar ends here -->
  </header>
  <div class="app-body">
    <div class="sidebar">
      @include('sidebar.sidebar-left')
      <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <main class="main">
      <!-- @include('sidebar.quickAccessBar') -->
      <div class="container-fluid">
        <div id="ui-view">
          <div class="">
            <div class="animated fadeIn">
              hihi write your contents here
            </div>
          </div>
        </div>
      </div>
    </main>
    @include('sidebar.sidebarRight')
  </div>

  @include('sidebar.footer')

  <script src="{{asset('coreui/vendors/jquery/js/jquery.min.js')}}"></script>
  <script src="{{asset('coreui/vendors/popper.js/js/popper.min.js')}}"></script>
  <script src="{{asset('coreui/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('coreui/vendors/pace-progress/js/pace.min.js')}}"></script>
  <script src="{{asset('coreui/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js')}}"></script>
  <script src="{{asset('coreui/vendors/coreui/coreui-pro/js/coreui.min.js')}}"></script>
  <script>
  // $('#ui-view').ajaxLoad();
  // $(document).ajaxComplete(function() {
  //   Pace.restart()
  // });
  </script>
</body>
</html>
