<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Session;
use Config;
use Auth;

class LoginController extends Controller
{
    //
    public function authenticate(Request $request)
    {
      // dd($request->all());
      $domainEmail = explode('@', $request['email']);
      if (isset($domainEmail)) {
        if (count($domainEmail) > 1) {
          $domainName = $domainEmail[1];
        } else {
          $domainName = '';
        }
      }

      $results = DB::connection('central')->table('domain_db')->where('domainName', $domainName)->get();

      // dd(count($results));

      if (count($results) != 1) {
        return back();
      }

      if ($results) {
        $dbName = $results[0]->dbName;
      } else {
        $dbName = '';
      }

      // dd($dbName);
      Session::put('domain', $dbName);

      Config::set('database.connections.tenant.database', $dbName);
      // dd(Session::all());
// dd(Config::get('database.connections.tenant'));
        $credentials = $request->only('email', 'password');
// dd($credentials);
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect(route('home'));
        } else {
          return redirect('/');
        }
    }

    public function logout()
    {
      // dd('here');
      Auth::logout();
      Session::flush();
      return redirect('/');
    }
}
