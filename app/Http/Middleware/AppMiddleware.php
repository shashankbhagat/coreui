<?php

namespace App\Http\Middleware;

use Closure;

use Session;
use Config;

class AppMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      Config::set('database.connections.tenant.database', Session::get('domain'));
        return $next($request);
    }
}
