<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
use Config;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // return $next($request);
      if (Auth::check()) {
        Config::set('database.default','tenant');
        return $next($request);
      } else {
        return redirect()->route('login');
      }
    }
}
