<ul class="nav">
  <li class="nav-title">Components</li>
  <li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
      <i class="nav-icon icon-puzzle"></i> Base</a>
      <ul class="nav-dropdown-items">
        <li class="nav-item">
          <a class="nav-link" href="base/tooltips.html">
            <i class="nav-icon icon-puzzle"></i> Tooltips</a>
          </li>
        </ul>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="nav-icon icon-pie-chart"></i> Charts</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="google-maps.html">
            <i class="nav-icon icon-map"></i> Google Maps
            <span class="badge badge-danger">PRO</span>
          </a>
        </li>
        <li class="nav-divider"></li>
        <li class="nav-title">Extras</li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon icon-star"></i> Pages</a>
            <ul class="nav-dropdown-items">
              <li class="nav-item">
                <a class="nav-link" href="login.html" target="_top">
                  <i class="nav-icon icon-star"></i> Login</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="register.html" target="_top">
                    <i class="nav-icon icon-star"></i> Register</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="404.html" target="_top">
                      <i class="nav-icon icon-star"></i> Error 404</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="500.html" target="_top">
                        <i class="nav-icon icon-star"></i> Error 500</a>
                      </li>
                    </ul>
                  </li>
                </ul>
