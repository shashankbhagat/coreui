<ul class="nav navbar-nav d-md-down-none">
  <li class="nav-item px-3">
    <a class="nav-link" href="#">Dashboard</a>
  </li>
  <li class="nav-item px-3">
    <a class="nav-link" href="#">Users</a>
  </li>
  <li class="nav-item px-3">
    <a class="nav-link" href="#">Settings</a>
  </li>
  <li class="nav-item px-3">
    <a class="nav-link text-danger" href="https://coreui.io/#sneak-peek"><strong>Sneak Peek! Try CoreUI PRO 3.0.0-alpha</strong></a>
  </li>
</ul>
